package com.example.hw002.service;

import com.example.hw002.model.entity.Product;
import com.example.hw002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Integer productId);

    boolean deleteProductById(Integer productId);

    Integer addNewProduct(ProductRequest productRequest);

    Integer updateProduct(ProductRequest productRequest, Integer productId);

}
