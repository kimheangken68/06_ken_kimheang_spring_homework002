package com.example.hw002.service.serviceImp;

import com.example.hw002.model.entity.Product;
import com.example.hw002.model.request.ProductRequest;
import com.example.hw002.repository.ProductRepository;
import com.example.hw002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId =productRepository.saveProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productIdUpdate = productRepository.updateProduct(productRequest,productId);
        return productIdUpdate;
    }


}
