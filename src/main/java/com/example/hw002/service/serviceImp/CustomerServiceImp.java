package com.example.hw002.service.serviceImp;

import com.example.hw002.model.entity.Customer;
import com.example.hw002.model.request.CustomerRequest;
import com.example.hw002.repository.CustomerRepository;
import com.example.hw002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getCustomerById(customerId);
    }

    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }

    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId =  customerRepository.saveCustomer(customerRequest);
        return customerId;
    }

    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerIdUpdate = customerRepository.updateCustomer(customerRequest,customerId);
        return customerIdUpdate;
    }
}
