package com.example.hw002.service.serviceImp;

import com.example.hw002.model.entity.Invoice;
import com.example.hw002.model.request.InvoiceRequest;
import com.example.hw002.repository.InvoiceRepository;
import com.example.hw002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.getInvoiceById(invoiceId);
    }

    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceById(invoiceId);
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {

        Integer storeInvoiceId = invoiceRepository.saveInvoice(invoiceRequest);

        for(Integer productId: invoiceRequest.getProductsId()
            ){
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId,productId);
        }
        return storeInvoiceId;
    }

    @Override
    public Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId) {
//        Integer storeUpdateInvoiceId = invoiceRepository.updateInvoice(invoiceRequest,invoiceId);
//        for (Integer productId : invoiceRequest.getProductsId()){
//            invoiceRepository.updateProductByInvoiceId(storeUpdateInvoiceId,productId);
//        }

        Integer storeUpdateInvoiceId = invoiceRepository.updateInvoiceById(invoiceRequest,invoiceId);
        if(storeUpdateInvoiceId == 1){
            invoiceRepository.deleteInvoiceInInvoiceDetailById(invoiceId);
            invoiceRequest.getProductsId().forEach(productId -> invoiceRepository.saveProductByInvoiceId(invoiceId, productId));
        }
        return storeUpdateInvoiceId;
    }
}
