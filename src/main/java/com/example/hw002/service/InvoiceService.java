package com.example.hw002.service;

import com.example.hw002.model.entity.Invoice;
import com.example.hw002.model.request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById (Integer invoiceId);

    boolean deleteInvoiceById(Integer invoiceId);

    Integer addNewInvoice(InvoiceRequest invoiceRequest);

    Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId);
}
