package com.example.hw002.service;

import com.example.hw002.model.entity.Customer;
import com.example.hw002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomers();
    Customer getCustomerById(Integer customerId);

    boolean deleteCustomerById(Integer customerId);

    Integer addNewCustomer(CustomerRequest customerRequest);

    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);
}
