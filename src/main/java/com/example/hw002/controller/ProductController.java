package com.example.hw002.controller;

import com.example.hw002.model.entity.Customer;
import com.example.hw002.model.entity.Product;
import com.example.hw002.model.request.CustomerRequest;
import com.example.hw002.model.request.ProductRequest;
import com.example.hw002.model.response.CustomResponse;
import com.example.hw002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    @Operation(summary = "Get all products")
    public ResponseEntity<CustomResponse<List<Product>>> getAllCustomer() {

        CustomResponse<List<Product>> response = CustomResponse.<List<Product>>builder()
                .message("Get all products successfully!")
                .payload(productService.getAllProducts())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("get-product-by-id/{id}")
    @Operation(summary = "Get product by id")
    public ResponseEntity<CustomResponse<Product>> getProductById(@PathVariable("id") Integer productId) {
        CustomResponse<Product> response;
        if (productService.getProductById(productId) != null) {
            response = CustomResponse.<Product>builder()
                    .message("Get customer by id successfully!")
                    .payload(productService.getProductById(productId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        } else {

            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete-product-by-id/{id}")
    @Operation(summary = "Delete product by id")
    public ResponseEntity<CustomResponse<String>> deleteProductById(@PathVariable("id") Integer productId){
        CustomResponse<String> response;
        if(productService.deleteProductById(productId)){
            response = CustomResponse.<String>builder()
                    .message("Delete this product is successful !!")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping("/add-new-product")
    @Operation(summary = "Save new product")
    public ResponseEntity<CustomResponse<Product>> addNewCustomer(@RequestBody ProductRequest productRequest){
        Integer storeProductId = productService.addNewProduct(productRequest);
        if(storeProductId != null){
            CustomResponse<Product> response = CustomResponse.<Product>builder()
                    .message("Add new product successfully!!")
                    .payload(productService.getProductById(storeProductId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/update-product-by-id/{id}")
    @Operation(summary = "Update product by Id")
    public ResponseEntity<CustomResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId
    ){
        CustomResponse<Product> response;
        Integer idProductUpdate = productService.updateProduct(productRequest,productId);
        if(idProductUpdate != null){
            response = CustomResponse.<Product>builder()
                    .message("Update this product Successfully!")
                    .payload(productService.getProductById(idProductUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return  ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }
    }


}
