package com.example.hw002.controller;

import com.example.hw002.model.entity.Customer;
import com.example.hw002.model.request.CustomerRequest;
import com.example.hw002.model.response.CustomResponse;
import com.example.hw002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Get all customers")
    public ResponseEntity<CustomResponse<List<Customer>>> getAllCustomer(){

        CustomResponse<List<Customer>> response = CustomResponse.<List<Customer>>builder()
                .message("Get all customers successfully!")
                .payload(customerService.getAllCustomers())
                .httpStatus(HttpStatus.OK)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping("get-customer-by-id/{id}")
    @Operation(summary = "Get customer by id")
    public ResponseEntity<CustomResponse<Customer>>  getCustomerById(@PathVariable("id") Integer customerId){
       CustomResponse<Customer> response;
        if(customerService.getCustomerById(customerId) != null){
             response = CustomResponse.<Customer>builder()
                    .message("Get customer by id successfully!")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        }else{

            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    @Operation(summary = "Delete customer by id")
    public ResponseEntity<CustomResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        CustomResponse<String> response;
        if(customerService.deleteCustomerById(customerId)){
            response = CustomResponse.<String>builder()
                    .message("Delete successfully")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping("/add-new-customer")
    @Operation(summary = "Save new customer")
    public ResponseEntity<CustomResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);
        if(customerService.addNewCustomer(customerRequest) != null){
            CustomResponse<Customer> response = CustomResponse.<Customer>builder()
                    .message("Add new customer successfully")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    @PutMapping("/update-customer-by-id/{id}")
    @Operation(summary = "Update customer by Id")
    public ResponseEntity<CustomResponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerId
    ){
        CustomResponse<Customer> response;
        Integer idCustomerUpdate = customerService.updateCustomer(customerRequest,customerId);
        if(idCustomerUpdate != null){
            response = CustomResponse.<Customer>builder()
                    .message("Updated Successfully!")
                    .payload(customerService.getCustomerById(idCustomerUpdate))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();

            return  ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }
    }
}
