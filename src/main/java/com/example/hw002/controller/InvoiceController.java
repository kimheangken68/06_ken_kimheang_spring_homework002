package com.example.hw002.controller;

import com.example.hw002.model.entity.Invoice;
import com.example.hw002.model.entity.Product;
import com.example.hw002.model.request.InvoiceRequest;
import com.example.hw002.model.request.ProductRequest;
import com.example.hw002.model.response.CustomResponse;
import com.example.hw002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {
    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("get-all-invoice")
    @Operation(summary = "Get all invoice")
    public ResponseEntity<List<Invoice>> getAllInvoice(){
        return ResponseEntity.ok(invoiceService.getAllInvoice());
    }

    @GetMapping("get-invoice-by-id/{id}")
    @Operation(summary = "Get invoice by ID")
    public ResponseEntity<CustomResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId ){

        CustomResponse<Invoice> response;
        if(invoiceService.getInvoiceById(invoiceId) != null){
            response = CustomResponse.<Invoice>builder()
                    .message("Get customer by id successfully!")
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);

        }else{

            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    @Operation(summary = "Delete invoice by id")
    public ResponseEntity<CustomResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId){
        CustomResponse<String> response;
        if(invoiceService.deleteInvoiceById(invoiceId)){
            response = CustomResponse.<String>builder()
                    .message("Delete this invoice is successful !!")
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }

    }

    @PostMapping("/add-new-invoice")
    @Operation(summary = "Add new invoice")
    public ResponseEntity<CustomResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        CustomResponse<Invoice> response;
        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);
        if(storeInvoiceId != null){
            response = CustomResponse.<Invoice>builder()
                    .message("Create invoice successfully!!")
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.notFound().build();
        }

    }

    @PutMapping("/update-invoice-by-id/{id}")
    @Operation(summary = "Update invoice by Id")
    public ResponseEntity<CustomResponse<Invoice>> updateInvoiceById(
            @RequestBody InvoiceRequest invoiceRequest, @PathVariable("id") Integer invoiceId
    ){
        CustomResponse<Invoice> response;
        Integer idInvoiceUpdate = invoiceService.updateInvoiceById(invoiceRequest,invoiceId);
        if(idInvoiceUpdate == 1){
            response = CustomResponse.<Invoice>builder()
                    .message("Update this product Successfully!")
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .httpStatus(HttpStatus.OK)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return  ResponseEntity.ok(response);
        }else {
            return ResponseEntity.notFound().build();
        }
    }






}
