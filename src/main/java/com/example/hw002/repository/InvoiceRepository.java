package com.example.hw002.repository;

import com.example.hw002.model.entity.Invoice;
import com.example.hw002.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice_tb ORDER BY invoice_id ASC")
    @Results(
        id = "invoiceMapper",
        value = {
            @Result(property = "invoice_id", column = "invoice_id"),
            @Result(property = "timestamp", column = "invoice_date"),
            @Result(property = "customer" , column = "customer_id",
                    one = @One(select = "com.example.hw002.repository.CustomerRepository.getCustomerById")
            ),
            @Result(property = "products", column = "invoice_id",
                    many = @Many(select = "com.example.hw002.repository.ProductRepository.getProductByInvoiceId")
            )
        }
    )
    List<Invoice> findAllInvoice();

    @Select("SELECT * FROM invoice_tb WHERE invoice_id=#{invoiceId}")
    @ResultMap("invoiceMapper")
    Invoice getInvoiceById(Integer invoiceId);

    @Delete("DELETE FROM invoice_tb WHERE invoice_id = #{invoiceId}")
    boolean deleteInvoiceById(Integer invoiceId);

    @Select("INSERT INTO invoice_tb (invoice_date, customer_id) "+
            "VALUES (#{request.invoice_date}, #{request.customer_id}) "+
            "RETURNING invoice_id"
    )
    Integer saveInvoice(@Param("request")InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoice_detail_tb (invoice_id, product_id) "+
            "VALUES (#{invoiceId},#{productId})"
    )
    void saveProductByInvoiceId(Integer invoiceId, Integer productId);
    @Update("UPDATE invoice_tb"+
            " SET invoice_date = #{request.invoice_date},"+
            " customer_id = #{request.customer_id}"+
            " WHERE invoice_id = #{invoiceId}")
    @ResultMap("invoiceMapper")
    Integer updateInvoiceById(@Param("request") InvoiceRequest invoiceRequest,Integer invoiceId);

    @Delete("DELETE FROM invoice_detail_tb WHERE invoice_id = #{invoiceId}")
    void deleteInvoiceInInvoiceDetailById(Integer invoiceId);
}
