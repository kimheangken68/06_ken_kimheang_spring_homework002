package com.example.hw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw002Application {

    public static void main(String[] args) {

        // Set the Elastic APM agent path
        System.setProperty("elastic.apm.agent.path", "./elastic-apm-agent-1.52.0.jar");

        SpringApplication.run(Hw002Application.class, args);
    }

}


