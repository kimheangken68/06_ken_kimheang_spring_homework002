CREATE TABLE customer_tb(
    customer_id SERIAL PRIMARY KEY ,
    customer_name VARCHAR (50),
    customer_address VARCHAR(50),
    customer_phone varchar(11)
);

CREATE TABLE product_tb (
    product_id SERIAL PRIMARY KEY,
    product_name VARCHAR(50) NOT NULL,
    product_price float4
);

CREATE TABLE invoice_detail_tb (
    id SERIAL PRIMARY KEY,
    invoice_id INT REFERENCES invoice_tb(invoice_id) ON UPDATE  CASCADE ON DELETE CASCADE,
    product_id INT REFERENCES product_tb(product_id) ON UPDATE  CASCADE ON DELETE CASCADE
);

CREATE TABLE invoice_tb(
    invoice_id SERIAL PRIMARY KEY,
    invoice_date TIMESTAMP,
    customer_id INT REFERENCES customer_tb(customer_id) ON UPDATE  CASCADE ON DELETE CASCADE
);